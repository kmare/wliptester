package com.weirdloop.wliptester;


import java.util.ArrayList;
import java.util.List;


class WLIP {

    // FF 14 00 06 00 85 86 00 00 00 68 65 6C 6C 6F 77 6F 72 6C 64 21

    private int numberOfRows;
    private byte command;
    private byte[] header = new byte[3];
    private byte[] dataHeader = new byte[20]; // won't be more than 20 lines
    private byte[] dataPayload = new byte[200];
    private String message;
    private String lines[];
    private int payloadLength = 0;

    private final char SOURCE_ID = 0x00;

    private final char NORMAL_MODE = 0x00;
    private final char DOUBLE_MODE = 0x01;
    private final char IMAGE_MODE = 0x02;

    private final char ACTION_KEEP = 0x00;
    private final char ACTION_CLEAR = 0x01;
    private final char ACTION_CHANGE = 0x02;

    WLIP(String msg, int numberOfRows) {
        this.message = msg;
        this.numberOfRows = numberOfRows;
    }

    private void generateHeader() {
        header[0] = (byte)(
                3 + // header size
                numberOfRows +  // number of lines on the display
                (payloadLength - (lines.length-1)) + // length of the payload, removing the \0A
                2 // CRC
        ); // length
        header[1] = dataControl(NORMAL_MODE); // data control
        header[2] = setupInfo(); // setup info
    }

    private byte dataControl(char mode) {
        return (byte)(mode & 0x0F);
    }

    private byte setupInfo() {
        return (byte)((SOURCE_ID << 5) | numberOfRows); // 0x06 for 'helloworld!'
    }

    private void generateDataHeader() {
        int i=0;
        for (String line : lines) {
            if (line.length() >0) {
                byte b = (byte) (ACTION_CHANGE << 6 | line.length());
                dataHeader[i] = b;
            } else {
                dataHeader[i] = 0x00;
            }
            i++;
        }
    }

    private void generatePayload() {
        dataPayload = this.message.getBytes();
    }

    String generate() {
        lines = this.message.split("\\r?\\n");

        for (String line : lines)
            this.payloadLength += line.length();
        payloadLength += lines.length-1;

        command = (byte)0xFF; // action command
        generateHeader();
        generateDataHeader();
        generatePayload();

        List<Byte> wlipMessage = new ArrayList<>();
        wlipMessage.add(command);
        for (byte b : header) {
            wlipMessage.add(b);
        }
        for (int i=0; i<numberOfRows; i++) {
            wlipMessage.add(dataHeader[i]);
        }
        for (int i=0; i<payloadLength; i++) {
            if (dataPayload[i] != 0x0A)
                wlipMessage.add(dataPayload[i]);
        }

        StringBuilder hex = new StringBuilder();
        for (int i=0; i<wlipMessage.size(); i++) {
            hex.append(String.format("%02X", wlipMessage.get(i)));
        }

        // add CRC
        String crc = getCRC16CCITT(hex.toString(), 0x1021, 0x0000, true);
        hex.append(crc);

        // encode with COBS (also adding 0x00 at the end)
        byte[] enc = Cobs.encode(hexStringToByteArray(hex.toString()));
        StringBuilder f = new StringBuilder();
        for (byte b : enc) {
            f.append(String.format("%02X", b));
        }

        return f.toString();
    }

    private static String getCRC16CCITT(String inputStr, int polynomial, int crc, boolean isHex) {

        int strLen = inputStr.length();
        int[] intArray;

        if (isHex) {
            if (strLen % 2 != 0) {
                inputStr = inputStr.substring(0, strLen - 1) + "0"
                        + inputStr.substring(strLen - 1, strLen);
                strLen++;
            }

            intArray = new int[strLen / 2];
            int ctr = 0;
            for (int n = 0; n < strLen; n += 2) {
                intArray[ctr] = Integer.valueOf(inputStr.substring(n, n + 2), 16);
                ctr++;
            }
        } else {
            intArray = new int[inputStr.getBytes().length];
            int ctr=0;
            for(byte b : inputStr.getBytes()){
                intArray[ctr] = b;
                ctr++;
            }
        }

        // generate the 16-bit CRC-CCITT xmodem
        for (int b : intArray) {
            for (int i = 0; i < 8; i++) {
                boolean bit = ((b >> (7 - i) & 1) == 1);
                boolean c15 = ((crc >> 15 & 1) == 1);
                crc <<= 1;
                if (c15 ^ bit)
                    crc ^= polynomial;
            }
        }

        crc &= 0xFFFF;
        StringBuilder crcStr = new StringBuilder(Integer.toHexString(crc).toUpperCase());
        int n = crcStr.length();
        for(int i=0; i<(4-n); i++){
            crcStr.insert(0, "0");
        }

        return crcStr.toString();
    }

    private static byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i+1), 16));
        }
        return data;
    }


}
