package com.weirdloop.wliptester;

import android.Manifest;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothProfile;
import android.content.pm.PackageManager;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.weirdloop.wliptester.adapters.BasicList;
import com.weirdloop.wliptester.libble.interfaces.BleCallback;
import com.weirdloop.wliptester.libble.models.BluetoothLE;
import com.weirdloop.wliptester.libble.utils.BluetoothLEHelper;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.os.Handler;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.UUID;

public class MainActivity extends AppCompatActivity {

    BluetoothLEHelper ble;
    AlertDialog dAlert;
    AlertDialog generateWlipAD;

    ListView listBle;
    TextView logTV;
    EditText inputET;
    ImageButton sendBtn;

    static String SERVICE = "49535343-fe7d-4ae5-8fa9-9fafd205e455";
    static String CHARACTERISTIC = "49535343-1e4d-4bd9-ba61-23c647249616";
    static String DESCRIPTOR = "00002902-0000-1000-8000-00805f9b34fb";

    int MY_PERMISSIONS_REQUEST_LOCATION = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(view -> Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show());
        fab.hide();


        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_CONTACTS)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.READ_CONTACTS)) {
                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }

        // Use this check to determine whether BLE is supported on the device.  Then you can
        // selectively disable BLE-related features.
        if (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            Toast.makeText(this, R.string.ble_not_supported, Toast.LENGTH_SHORT).show();
            finish();
        }

        //--- Initialize BLE Helper
        ble = new BluetoothLEHelper(this);
        ble.setScanPeriod(5000);

        listBle  = findViewById(R.id.listBle);
        logTV = findViewById(R.id.logTV);
        inputET = findViewById(R.id.inputET);
        sendBtn = findViewById(R.id.sendBtn);

        logTV.setMovementMethod(new ScrollingMovementMethod());
        logTV.setVisibility(View.VISIBLE);
        listBle.setVisibility(View.GONE);

        sendBtn.setOnClickListener(v -> {
            if(ble.isConnected()) {
                // byte[] aBytes = new byte[8];
                // byte[] aBytes = {0x77, 0x65, 0x69, 0x72, 0x64, 0x6c, 0x6f, 0x6f, 0x70};
                CharSequence input = inputET.getText();
                byte[] bytes = input.toString().getBytes();
                ble.write(SERVICE, CHARACTERISTIC, bytes);
            }
        });

        // only scan this service???
        // ble.setFilterService(SERVICE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if (requestCode == MY_PERMISSIONS_REQUEST_LOCATION) {
            // If request is cancelled, the result arrays are empty.
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

            } else {
            }
            return;
        }

        // other 'case' lines to check for other
        // permissions this app might request
    }

    private AlertDialog generateWLIP(String title, boolean btnVisible){

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_wlip, null);

        TextView btnNeutral = view.findViewById(R.id.btnNeutralWL);
        TextView txtTitle   = view.findViewById(R.id.txtTitleWL);

        EditText message = view.findViewById(R.id.wlipMessageET);
        TextView generatedWlip = view.findViewById(R.id.generatedWlipTV);

        message.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // String msg = new String(s);
                WLIP wlip = new WLIP(s.toString(), 6);
                generatedWlip.setText(wlip.generate());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        txtTitle.setText(title);

        if(btnVisible){
            btnNeutral.setVisibility(View.VISIBLE);
        }else{
            btnNeutral.setVisibility(View.GONE);
        }

        btnNeutral.setOnClickListener(view1 -> {
            generateWlipAD.dismiss();
            // TODO: send the generated message
            byte[] msg = hexToBinary(generatedWlip.getText().toString());
            ble.write(SERVICE, CHARACTERISTIC, msg);
        });

        builder.setView(view);
        return builder.create();
    }

    private AlertDialog setDialogInfo(String title, String message, boolean btnVisible){

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_standard, null);

        TextView btnNeutral = view.findViewById(R.id.btnNeutral);
        TextView txtTitle   = view.findViewById(R.id.txtTitle);
        TextView txtMessage = view.findViewById(R.id.txtMessage);

        txtTitle.setText(title);
        txtMessage.setText(message);

        if(btnVisible){
            btnNeutral.setVisibility(View.VISIBLE);
        }else{
            btnNeutral.setVisibility(View.GONE);
        }

        btnNeutral.setOnClickListener(view1 -> dAlert.dismiss());

        builder.setView(view);
        return builder.create();
    }

    private void setList(){

        ArrayList<BluetoothLE> aBleAvailable  = new ArrayList<>();

        if(ble.getListDevices().size() > 0){
            for (int i=0; i<ble.getListDevices().size(); i++) {
                aBleAvailable.add(new BluetoothLE(ble.getListDevices().get(i).getName(), ble.getListDevices()
                        .get(i).getMacAddress(), ble.getListDevices().get(i).getRssi(), ble.getListDevices().get(i).getDevice()));
            }

            BasicList mAdapter = new BasicList(this, R.layout.simple_row_list, aBleAvailable) {
                @Override
                public void onItem(Object item, View view, int position) {
                    TextView txtName = view.findViewById(R.id.txtText);
                    String aux = ((BluetoothLE) item).getName() + "    " + ((BluetoothLE) item).getMacAddress();
                    txtName.setText(aux);
                }
            };

            listBle.setAdapter(mAdapter);
            listBle.setOnItemClickListener((parent, view, position, id) -> {
                BluetoothLE  itemValue = (BluetoothLE) listBle.getItemAtPosition(position);
                Log.d("TAG", "Is it connected: " + ble.isConnected() + ", to dev: " + itemValue.getDevice());
                ble.connect(itemValue.getDevice(), bleCallbacks());
            });
        }else{
            dAlert = setDialogInfo("Wooops!", "No active devices found :(", true);
            dAlert.show();
        }
    }

    private BleCallback bleCallbacks(){

        return new BleCallback(){

            @Override
            public void onBleConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
                super.onBleConnectionStateChange(gatt, status, newState);

                if (newState == BluetoothProfile.STATE_CONNECTED) {
                    runOnUiThread(() -> {
                        Toast.makeText(MainActivity.this, "Connected to GATT server.", Toast.LENGTH_SHORT).show();
                        Log.d("TAG", "Connected: " + ble.isConnected());
                        logTV.setVisibility(View.VISIBLE);
                        listBle.setVisibility(View.GONE);
                        // MainActivity.this.findViewById(R.id.action_disconnect).setVisibility(View.VISIBLE);
                        // MainActivity.this.findViewById(R.id.action_scan).setVisibility(View.GONE);
                    });
                }

                if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                    runOnUiThread(() -> {
                        Toast.makeText(MainActivity.this, "Disconnected from GATT server.", Toast.LENGTH_SHORT).show();
                        // MainActivity.this.findViewById(R.id.action_disconnect).setVisibility(View.GONE);
                        // MainActivity.this.findViewById(R.id.action_scan).setVisibility(View.VISIBLE);
                        Log.d("TAG", "Not Connected: " + ble.isConnected());
                    });
                }
            }

            @Override
            public void onBleServiceDiscovered(BluetoothGatt gatt, int status) {
                super.onBleServiceDiscovered(gatt, status);
                if (status != BluetoothGatt.GATT_SUCCESS) {
                    // that's actually an error, let's handle that!
                    Log.e("Ble ServiceDiscovered","onServicesDiscovered received: " + status);
                }

                // Get the counter characteristic
                BluetoothGattCharacteristic characteristic = gatt
                        .getService(UUID.fromString(SERVICE))
                        .getCharacteristic(UUID.fromString(CHARACTERISTIC));

                // Enable notifications for this characteristic locally
                gatt.setCharacteristicNotification(characteristic, true);

                /*for (BluetoothGattDescriptor descriptor:characteristic.getDescriptors()){
                    Log.e("TAG", "BluetoothGattDescriptor: "+descriptor.getUuid().toString());
                }*/
                BluetoothGattDescriptor descriptor =
                        characteristic.getDescriptor(UUID.fromString(DESCRIPTOR)); // 0x2902??
                descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
                gatt.writeDescriptor(descriptor);
            }

            @Override
            public void onBleCharacteristicChange(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
                super.onBleCharacteristicChange(gatt, characteristic);
                Log.d("TAG", "characteristic change called!!");
                Log.i("BluetoothLEHelper","onCharacteristicChanged Value: " + Arrays.toString(characteristic.getValue()));
                // String str = logTV.getText() + new String(characteristic.getValue(), StandardCharsets.UTF_8); // for UTF-8 encoding
                // logTV.setText(str);

                String r = new String(characteristic.getValue(), StandardCharsets.UTF_8);
                if (! r.equals("\r") ) {
                    runOnUiThread(() -> {
                        String alien = "<b>alien: </b> ";
                        logTV.append(Html.fromHtml(alien));
                        logTV.append(new String(characteristic.getValue(), StandardCharsets.UTF_8));
                        logTV.append("\n");
                    });
                }


            }

            @Override
            public void onBleRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
                super.onBleRead(gatt, characteristic, status);
                Log.d("TAG", "read callback!");

                if (status == BluetoothGatt.GATT_SUCCESS) {
                    Log.i("TAG", Arrays.toString(characteristic.getValue()));
                    runOnUiThread(() -> Toast.makeText(MainActivity.this, "onCharacteristicRead : "+Arrays.toString(characteristic.getValue()), Toast.LENGTH_SHORT).show());
                }
            }

            @Override
            public void onBleWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
                super.onBleWrite(gatt, characteristic, status);
                // runOnUiThread(() -> Toast.makeText(MainActivity.this, "onCharacteristicWrite Status : " + status, Toast.LENGTH_SHORT).show());
                runOnUiThread(() -> {
                    if (!inputET.getText().toString().equals("")) {
                        String me = "<b>me: </b> ";
                        logTV.append(Html.fromHtml(me));
                        logTV.append(inputET.getText());
                        logTV.append("\n");
                        inputET.setText("");
                    }
                });
            }

        };
    }

    private void scanCollars(){

        if(!ble.isScanning()) {

            dAlert = setDialogInfo("Scan in progress", "Loading...", false);
            dAlert.show();

            Handler mHandler = new Handler();
            ble.scanLeDevice(true);

            mHandler.postDelayed(() -> {
                dAlert.dismiss();
                logTV.setVisibility(View.GONE);
                listBle.setVisibility(View.VISIBLE);
                setList();
            }, ble.getScanPeriod());

        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ble.disconnect();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        if (id == R.id.action_scan) {
            if(ble.isReadyForScan()){
                scanCollars();
            }else{
                Toast.makeText(MainActivity.this, "You must accept the bluetooth and Gps permissions or must turn on the bluetooth and Gps", Toast.LENGTH_SHORT).show();
            }
            return true;
        }

        if (id == R.id.action_disconnect) {
            if(ble.isConnected()){
                ble.disconnect();
                Log.d("TAG", "Disconnecting: " + ble.isConnected());
                // MainActivity.this.findViewById(R.id.action_disconnect).setVisibility(View.GONE);
            }else{
                Toast.makeText(MainActivity.this, "You are *not* connected", Toast.LENGTH_SHORT).show();
            }
            return true;
        }

        if (id== R.id.action_wlip) {
            if (ble.isConnected()) {
                generateWlipAD = generateWLIP("Generate WLIP!", true);
                generateWlipAD.show();
            }
        }

        if (id == R.id.action_write) {
            if(ble.isConnected()) {
                // byte[] aBytes = new byte[8];
                byte[] aBytes = {0x77, 0x65, 0x69, 0x72, 0x64, 0x6c, 0x6f, 0x6f, 0x70};
                ble.write(SERVICE, CHARACTERISTIC, aBytes);
            }
            return true;
        }

        if (id == R.id.action_read) {
            if(ble.isConnected()) {
                ble.read(SERVICE, CHARACTERISTIC);
                Log.d("TAG", "read button pushed");
            }
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public static byte[] hexToBinary(String s){

        /*
         * skipped any input validation code
         */

        byte[] data = new byte[s.length()/2];

        for( int i=0, j=0;
             i<s.length() && j<data.length;
             i+=2, j++)
        {
            data[j] = (byte)Integer.parseInt(s.substring(i, i+2), 16);
        }

        return data;
    }
}
