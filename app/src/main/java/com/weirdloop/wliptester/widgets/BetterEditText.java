package com.weirdloop.wliptester.widgets;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.EditText;

/**
 * Created by kmare on 11/10/19.
 * Belongs to WLIPTester
 */
public class BetterEditText extends EditText {

    private final Context context;
    private Rect rect;
    private Paint paint;

    public BetterEditText(Context context)
    {
        super(context);
        this.context = context;
        init();
    }

    public BetterEditText(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        this.context = context;
        init();
    }

    public BetterEditText(Context context, AttributeSet attrs, int defStyle)
    {
        super(context, attrs, defStyle);
        this.context=context;
        init();
    }

    private void init()
    {
        rect = new Rect();
        paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(Color.MAGENTA);
        int spSize = 12; // font size in sp
        float scaledTextSize = spSize * getResources().getDisplayMetrics().scaledDensity; // convert the sp to pixels
        paint.setTextSize(scaledTextSize);
        paint.setTypeface(Typeface.MONOSPACE);
    }


    @Override
    protected void onDraw(Canvas canvas) {

        int baseline;
        int lineCount = getLineCount();
        int lineNumber = 1;

        for (int i = 0; i < lineCount; ++i) {
            baseline=getLineBounds(i, null);
            if (i == 0) {
                canvas.drawText(""+lineNumber, rect.left, baseline, paint);
                ++lineNumber;
            }
            else if (getText().charAt(getLayout().getLineStart(i) - 1) == '\n')
            {
                canvas.drawText(""+lineNumber, rect.left, baseline, paint);
                ++lineNumber;
            }
        }

        // for setting edittext start padding
        if(lineCount<100) {
            setPadding(40,getPaddingTop(),getPaddingRight(),getPaddingBottom());
        }
        else if(lineCount>99 && lineCount<1000) {
            setPadding(50,getPaddingTop(),getPaddingRight(),getPaddingBottom());
        }
        else if(lineCount>999 && lineCount<10000) {
            setPadding(60,getPaddingTop(),getPaddingRight(),getPaddingBottom());
        }
        else if(lineCount>9999 && lineCount<100000) {
            setPadding(70,getPaddingTop(),getPaddingRight(),getPaddingBottom());
        }

        super.onDraw(canvas);
    }
}
